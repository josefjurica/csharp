﻿using System;
using Fei;
using System.Linq;

namespace Cvic2.ukol2
{
    class Program
    {
        static int[] a = new int[10];
        static void Main(string[] args)
        {
            
            //Console.WriteLine("MENU:\n1.Zadaní prvků pole z klávesnice\n2.Výpis pole na obrazovku\n3.Utřídění pole vzestupně\n4.Hledání minimálního prvku\n5.Hledání prvního výskytu zadaného čísla\n6.Hledání posledního výskytu zadaného čísla\n7.Konec programu");
            int moznost = 0;
            while (moznost != 7)
            {
                Console.WriteLine("Menu:");
                Console.WriteLine("1 - zadání prvků pole z klávesnice");
                Console.WriteLine("2 - výpis pole na obrazovku");
                Console.WriteLine("3 - utřídění pole vzestupně");
                Console.WriteLine("4 - hledání minimálního prvku");
                Console.WriteLine("5 - hledání prvního výskytu zadaného čísla");
                Console.WriteLine("6 - hledání posledního výskytu zadaného čísla");
                Console.WriteLine("7 - konec programu");
                Console.WriteLine("8 - setřídit pole sestupně");
                try
                {
                    moznost = Reading.ReadInt("zadejte volbu");
                }
                catch (Exception)
                {
                    continue;
                }
                


                if (moznost == 1)
                {
                    for (int i = 0; i < a.Length; i++)
                    {
                        a[i] = Reading.ReadInt("zadejte číslo na indexu " + i);
                    }
                }
                if (moznost == 2)
                {
                    for (int i = 0; i < a.Length; i++)
                    {
                        Console.WriteLine(a[i]);
                    }
                }
                if (moznost == 3)
                {                   
                        Array.Sort(a);
                }
                if (moznost == 4)
                {                
                        Console.WriteLine("minimum je: " + a.Min());

                }
                if (moznost == 5)
                {
                        int n = 0;
                        n = Reading.ReadInt("zadejte číslo");
                        Console.WriteLine("číslo se vyskytuje na indexu:" + Array.IndexOf(a, n));
                }
                if (moznost == 6)
                {
                    int n = 0;
                    n = Reading.ReadInt("zadejte číslo");
                    Console.WriteLine("číslo se vyskytuje na indexu: " + Array.LastIndexOf(a, n));
                }
                if (moznost == 8)
                {
                   
                    Array.Reverse(a);
                }
            }
        }

       

    }
    }

