﻿using System;

namespace Fei
{
    public class Reading
    {
        public static double ReadDouble(string t)
        {
            /// <summary>
            /// vypíše zadaný text s dvojtečkou a načte double
            /// </summary>
            /// <param name="t">zadaná text</param>
            /// <returns>double nebo vyjímka</returns>
            Console.WriteLine(t + ": ");
            string text = Console.ReadLine();
            double d;
            if (Double.TryParse(text, out d))
            {
                return d;
            }
            else
            {
                throw new Exception();
            }
        }
        /// <summary>
        /// vypíše zadaný text s dvojtečkou a načte int
        /// </summary>
        /// <param name="t">zadaný text</param>
        /// <returns>int nebo vyjímka</returns>
        public static int ReadInt(string t)
        {
            Console.WriteLine(t + ": ");
            string text = Console.ReadLine();
            int i;
            if (int.TryParse(text, out i))
            {
                return i;
            }
            else
            {
                throw new Exception();
            }
        }


        /// <summary>
        /// vypíše zadaný text s dvojtečkou a načte char
        /// </summary>
        /// <param name="t">hláška k vypsání</param>
        /// <returns>char</returns>
        public static char ReadChar(string t)
        {
            Console.WriteLine(t + ": ");
            return Console.ReadKey().KeyChar;
        }


        /// <summary>
        /// vypíše zadaný text s dvojtečkou a načte string
        /// </summary>
        /// <param name="t">hláška k vypsání</param>
        /// <returns>string</returns>
        public static string ReadString(string t)
        {
            Console.WriteLine(t + ": ");
            return Console.ReadLine();
        }

        
    }
}