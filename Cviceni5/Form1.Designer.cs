﻿
namespace Cviceni5
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.Vymaž = new System.Windows.Forms.Button();
            this.Upravit = new System.Windows.Forms.Button();
            this.Přidej = new System.Windows.Forms.Button();
            this.Nejlepší = new System.Windows.Forms.Button();
            this.Registrovat = new System.Windows.Forms.Button();
            this.Zrušit = new System.Windows.Forms.Button();
            this.Konec = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // Vymaž
            // 
            this.Vymaž.Location = new System.Drawing.Point(515, 56);
            this.Vymaž.Name = "Vymaž";
            this.Vymaž.Size = new System.Drawing.Size(75, 23);
            this.Vymaž.TabIndex = 1;
            this.Vymaž.Text = "Vymaž";
            this.Vymaž.UseVisualStyleBackColor = true;
            this.Vymaž.Click += new System.EventHandler(this.Vymaž_Click);
            // 
            // Upravit
            // 
            this.Upravit.Location = new System.Drawing.Point(515, 85);
            this.Upravit.Name = "Upravit";
            this.Upravit.Size = new System.Drawing.Size(75, 23);
            this.Upravit.TabIndex = 2;
            this.Upravit.Text = "Upravit..";
            this.Upravit.UseVisualStyleBackColor = true;
            this.Upravit.Click += new System.EventHandler(this.Upravit_Click);
            // 
            // Přidej
            // 
            this.Přidej.Location = new System.Drawing.Point(515, 27);
            this.Přidej.Name = "Přidej";
            this.Přidej.Size = new System.Drawing.Size(75, 23);
            this.Přidej.TabIndex = 3;
            this.Přidej.Text = "Přidej";
            this.Přidej.UseVisualStyleBackColor = true;
            this.Přidej.Click += new System.EventHandler(this.Přidej_Click);
            // 
            // Nejlepší
            // 
            this.Nejlepší.Location = new System.Drawing.Point(515, 114);
            this.Nejlepší.Name = "Nejlepší";
            this.Nejlepší.Size = new System.Drawing.Size(75, 23);
            this.Nejlepší.TabIndex = 4;
            this.Nejlepší.Text = "Nejlepší..";
            this.Nejlepší.UseVisualStyleBackColor = true;
            this.Nejlepší.Click += new System.EventHandler(this.Nejlepší_Click);
            // 
            // Registrovat
            // 
            this.Registrovat.Location = new System.Drawing.Point(515, 143);
            this.Registrovat.Name = "Registrovat";
            this.Registrovat.Size = new System.Drawing.Size(75, 23);
            this.Registrovat.TabIndex = 5;
            this.Registrovat.Text = "Registrovat";
            this.Registrovat.UseVisualStyleBackColor = true;
            this.Registrovat.Click += new System.EventHandler(this.Registrovat_Click);
            // 
            // Zrušit
            // 
            this.Zrušit.Location = new System.Drawing.Point(515, 172);
            this.Zrušit.Name = "Zrušit";
            this.Zrušit.Size = new System.Drawing.Size(75, 23);
            this.Zrušit.TabIndex = 6;
            this.Zrušit.Text = "Zrušit";
            this.Zrušit.UseVisualStyleBackColor = true;
            this.Zrušit.Click += new System.EventHandler(this.Zrušit_Click);
            // 
            // Konec
            // 
            this.Konec.Location = new System.Drawing.Point(515, 201);
            this.Konec.Name = "Konec";
            this.Konec.Size = new System.Drawing.Size(75, 23);
            this.Konec.TabIndex = 7;
            this.Konec.Text = "Konec";
            this.Konec.UseVisualStyleBackColor = true;
            this.Konec.Click += new System.EventHandler(this.Konec_Click);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(25, 27);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(471, 277);
            this.listBox1.TabIndex = 8;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Location = new System.Drawing.Point(25, 327);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(471, 82);
            this.listBox2.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 450);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.Konec);
            this.Controls.Add(this.Zrušit);
            this.Controls.Add(this.Registrovat);
            this.Controls.Add(this.Nejlepší);
            this.Controls.Add(this.Přidej);
            this.Controls.Add(this.Upravit);
            this.Controls.Add(this.Vymaž);
            this.Name = "Form1";
            this.Text = "LigaMistrů";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Vymaž;
        private System.Windows.Forms.Button Upravit;
        private System.Windows.Forms.Button Přidej;
        private System.Windows.Forms.Button Nejlepší;
        private System.Windows.Forms.Button Registrovat;
        private System.Windows.Forms.Button Zrušit;
        private System.Windows.Forms.Button Konec;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListBox listBox2;
    }
}

