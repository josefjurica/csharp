﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cviceni5
{
   

    public partial class Form1 : Form
    {
        private Hraci.PocetHracuChangeEventHandler handler;
        public static Hraci hraci = new Hraci();
        public string[] kluby;
        int pocetGolu;
        public Form1()
        {
            InitializeComponent();
            handler = delegate () {
                listBox2.Items.Add("Proběhla změna počtu nebo informace o hráčích");
                listBox1.Items.Clear();
                foreach (Hrac hrac in hraci.seznam)
                {
                    if (hrac != null)
                    {
                        listBox1.Items.Add(hrac);
                    }

                }
                hraci.seznam.Reset();
            };
        }

        private void Přidej_Click(object sender, EventArgs e)
        {
            FormHráč formhrac = new FormHráč(null);
            formhrac.ShowDialog();
            if (formhrac.DialogResult == DialogResult.OK)
            {
                hraci.Pridej(formhrac.Hrac);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Vymaž_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                hraci.Vymaz((Hrac)listBox1.SelectedItem);
            }
        }

        private void Upravit_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedItem != null)
            {
                FormHráč formhrac = new FormHráč((Hrac)listBox1.SelectedItem);
                formhrac.ShowDialog();
                if (formhrac.DialogResult == DialogResult.OK)
                {
                    hraci.Vymaz((Hrac)listBox1.SelectedItem);
                    hraci.Pridej(formhrac.Hrac);
                }
            }
        }

        private void Zrušit_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen -= handler;
            listBox2.Items.Add("Zrušená registrace.");
        }

        private void Konec_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Nejlepší_Click(object sender, EventArgs e)
        {
            hraci.NajdiNejlepsiKluby(out kluby, out pocetGolu);
            FormKluby formKlub = new FormKluby(kluby, pocetGolu);
            formKlub.ShowDialog();
        }

        private void Registrovat_Click(object sender, EventArgs e)
        {
            hraci.PocetZmenen += handler;
            listBox2.Items.Add("Zahájena zaregistrace.");
        }
    }
}
