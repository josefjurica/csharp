﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cviceni5
{
    public partial class FormKluby : Form
    {
        public FormKluby(string[] kluby, int gol)
        {
            InitializeComponent();
            textBox1.Text = gol.ToString();
            for (int i = 0; i < kluby.Length; i++)
            {
                listBox1.Items.Add(kluby[i].ToString());

            }
        }

        private void FormKluby_Load(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
