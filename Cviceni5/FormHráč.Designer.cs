﻿
namespace Cviceni5
{
    partial class FormHráč
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.jmeno = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.goly = new System.Windows.Forms.TextBox();
            this.OK = new System.Windows.Forms.Button();
            this.Storno = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Jméno";
            // 
            // jmeno
            // 
            this.jmeno.Location = new System.Drawing.Point(78, 21);
            this.jmeno.Name = "jmeno";
            this.jmeno.Size = new System.Drawing.Size(121, 20);
            this.jmeno.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Klub";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "FC Porto",
            "Arsenal",
            "Real Madrid",
            "Chelsea",
            "Barcelona"});
            this.comboBox1.Location = new System.Drawing.Point(78, 64);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 3;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(28, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Góly";
            // 
            // goly
            // 
            this.goly.Location = new System.Drawing.Point(78, 112);
            this.goly.Name = "goly";
            this.goly.Size = new System.Drawing.Size(121, 20);
            this.goly.TabIndex = 5;
            // 
            // OK
            // 
            this.OK.Location = new System.Drawing.Point(36, 162);
            this.OK.Name = "OK";
            this.OK.Size = new System.Drawing.Size(75, 23);
            this.OK.TabIndex = 6;
            this.OK.Text = "OK";
            this.OK.UseVisualStyleBackColor = true;
            this.OK.Click += new System.EventHandler(this.OK_Click);
            // 
            // Storno
            // 
            this.Storno.Location = new System.Drawing.Point(165, 162);
            this.Storno.Name = "Storno";
            this.Storno.Size = new System.Drawing.Size(75, 23);
            this.Storno.TabIndex = 7;
            this.Storno.Text = "Storno";
            this.Storno.UseVisualStyleBackColor = true;
            this.Storno.Click += new System.EventHandler(this.Storno_Click);
            // 
            // Hráč
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(304, 206);
            this.Controls.Add(this.Storno);
            this.Controls.Add(this.OK);
            this.Controls.Add(this.goly);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.jmeno);
            this.Controls.Add(this.label1);
            this.Name = "Hráč";
            this.Text = "Hráč";
            this.Load += new System.EventHandler(this.Hráč_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox jmeno;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox goly;
        private System.Windows.Forms.Button OK;
        private System.Windows.Forms.Button Storno;
    }
}