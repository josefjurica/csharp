﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cviceni5
{
    public partial class FormHráč : Form
    {
        public Hrac Hrac { get; set; }

        public FormHráč(Hrac hrac)
        {
            InitializeComponent();
            comboBox1.DataSource = Enum.GetValues(typeof(FotbalovyKlub));
            if (hrac != null)
            {
                jmeno.Text = hrac.Jmeno;
                comboBox1.SelectedItem = hrac.Klub;
                goly.Text = hrac.GolPocet.ToString();
            }
        }

        private void progressBar1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Hráč_Load(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {
            FotbalovyKlub f;
            if (jmeno.Text != null && Enum.TryParse<FotbalovyKlub>(comboBox1.SelectedValue.ToString(), out f) != false && goly.Text != "")
            {
                Hrac = new Hrac(jmeno.Text, f, Int32.Parse(goly.Text));
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                MessageBox.Show("Špatně zadané údaje", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void Storno_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
