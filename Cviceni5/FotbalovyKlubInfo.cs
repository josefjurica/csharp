﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cviceni5
{
    public enum FotbalovyKlub
    {
        None,
        Porto,
        Arsenal,
        Real,
        Chelsea,
        Barcelona
    }

    class FotbalovyKlubInfo
    {
        public static int Pocet { get; } = 6;
        public static string DejNazev(FotbalovyKlub name)
        {
            return Enum.GetName(typeof(FotbalovyKlub), name);
        }
        public static FotbalovyKlub DejTyp(string jmeno)
        {
            switch (jmeno)
            {
                case "None":
                    return FotbalovyKlub.None;
                case "Porto":
                    return FotbalovyKlub.Porto;
                case "Arsenal":
                    return FotbalovyKlub.Arsenal;
                case "Real":
                    return FotbalovyKlub.Real;
                case "Chelsea":
                    return FotbalovyKlub.Chelsea;
                case "Barcelona":
                    return FotbalovyKlub.Barcelona;
                default:
                    return FotbalovyKlub.None;
            }
        }
    }
}
