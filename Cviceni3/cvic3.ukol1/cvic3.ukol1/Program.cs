﻿using System;
using Fei.BaseLib;


namespace Cvic3.ukol1
{
    public enum FakultaEnum { FES, FF, FEI, FCHT }
    public class Student
    {
        private string jmeno;

        public string Jmeno
        {
            get { return jmeno; }
            set { jmeno = value; }
        }

        private int cislo;

        public int Cislo
        {
            get { return cislo; }
            set { cislo = value; }
        }



        public FakultaEnum Fakulta { get; set; }
    }

    class Program
    {
        static int pocetStudentu = 3;
        static Student[] Students = new Student[pocetStudentu];


        static void Main(string[] args)
        {

            int menu = 0;
            while (!(menu == 6))
            {

                Console.WriteLine(@"1) Načtení studentů z klávesnice
2) Výpis studentů na obrazovku
3) Seřazení studentů podle čísla
4) Seřazení studentů podle jména
5) Seřazení studentů podle fakulty
0) Konec programu");
                try
                {
                    menu = Reading.ReadInt("Zadej cislo z meny");
                }
                catch (Exception)
                {
                    Console.WriteLine("Špatně zadaná hodnota.");
                    continue;
                }
               

                switch (menu)
                {
                    case 1:
                        Nacti();
                        break;
                    case 2:
                        Vypis();
                        break;
                    case 3:
                        SeradCisla();
                        break;
                    case 4:
                        SeradJmena();
                        break;
                    case 5:
                        SeradFakulty();
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }





                //Students[0].Cislo = 1;
                //Students[0].Fakulta = 1;
                //Console.WriteLine(a);      
            }
        }
        public static void Nacti()
        {

            int cislo = 0;
            for (int i = 0; i < pocetStudentu; i++)
            {
                try
                {
                    cislo = Reading.ReadInt("Zadej cislo studenta");
                }
                catch (Exception)
                {

                    //cislo = Reading.ReadInt("Zadej cislo studenta");
                    continue;
                }



                Console.WriteLine("Zadej jmeno:");
                string jmeno = Console.ReadLine();

                for (int j = 0; j < 4; j++)
                {
                    Console.WriteLine($"{j + 1} = {(FakultaEnum)j}");
                }
                int b = -1;
                while (!(b < 5 && b > 0))
                {
                    b = Reading.ReadInt("Zadej cislo fakulty");
                }

                Students[i] = new Student
                {
                    Jmeno = jmeno,
                    Cislo = cislo,
                    Fakulta = (FakultaEnum)(b - 1),
                };
            }
        }

        public static void Vypis()
        {
            for (int i = 0; i < pocetStudentu; i++)
            {
                try
                {
                    Console.WriteLine($"{Students[i].Cislo} {Students[i].Jmeno}  {Students[i].Fakulta}");
                }
                catch (Exception)
                {
                    Console.WriteLine("špatně zadaní studenti");
                    continue;
                }

            }
        }
        public static void SeradCisla()
        {
            Array.Sort(Students, delegate (Student student1, Student student2)
            {
                return student1.Cislo.CompareTo(student2.Cislo);
            });
        }

        public static void SeradJmena()
        {
            Array.Sort(Students, delegate (Student student1, Student student2)
            {
                return student1.Jmeno.CompareTo(student2.Jmeno);
            });
        }

        public static void SeradFakulty()
        {
            Array.Sort(Students, delegate (Student student1, Student student2)
            {
                return student1.Fakulta.CompareTo(student2.Fakulta);
            });
        }
    }

}
