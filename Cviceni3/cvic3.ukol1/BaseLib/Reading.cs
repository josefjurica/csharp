﻿using System;

namespace Fei
{
    namespace BaseLib
    {
        public class Reading
        {
            public static double ReadDouble(string t)
            {
                /// <summary>
                /// vypíše zadaný text s dvojtečkou a načte double
                /// </summary>
                /// <param name="t">zadaná text</param>
                /// <returns>double nebo vyjímka</returns>
                Console.WriteLine(t + ": ");
                string text = Console.ReadLine();
                double d;
                if (Double.TryParse(text, out d))
                {
                    return d;
                }
                else
                {
                    throw new Exception();
                }
            }
            /// <summary>
            /// vypíše zadaný text s dvojtečkou a načte int
            /// </summary>
            /// <param name="t">zadaný text</param>
            /// <returns>int nebo vyjímka</returns>
            public static int ReadInt(string t)
            {
                Console.WriteLine(t + ": ");
                string text = Console.ReadLine();
                int i;
                if (int.TryParse(text, out i))
                {
                    return i;
                }
                else
                {
                    throw new Exception();
                }
            }


            /// <summary>
            /// vypíše zadaný text s dvojtečkou a načte char
            /// </summary>
            /// <param name="t">hláška k vypsání</param>
            /// <returns>char</returns>
            public static char ReadChar(string t)
            {
                Console.WriteLine(t + ": ");
                return Console.ReadKey().KeyChar;
            }


            /// <summary>
            /// vypíše zadaný text s dvojtečkou a načte string
            /// </summary>
            /// <param name="t">hláška k vypsání</param>
            /// <returns>string</returns>
            public static string ReadString(string t)
            {
                Console.WriteLine(t + ": ");
                return Console.ReadLine();
            }


        }

        public class ExtraMath
        {
            /// <summary>
            /// třída pro řešení kvadratické rovnice
            /// </summary>
            /// <param name="a"></param>
            /// <param name="b"></param>
            /// <param name="c"></param>
            /// <returns>žádné řešení - null, jinak pole s kořeny</returns>
            public static double[] KvadratickaRovnice(double a, double b, double c)
            {
                double[] result = new double[2];
                double x1;
                double x2;
                double dis = Math.Pow(b, 2) - (4 * a * c);
                Console.WriteLine(dis);
                if (dis < 0)
                {
                    return null;
                }
                else if (dis == 0)
                {
                    x1 = (-b) / (2 * a);
                    result[0] = x1;
                    return result;
                }
                else
                {
                    x1 = (-b + Math.Sqrt(dis)) / (2 * a);
                    x2 = (-b - Math.Sqrt(dis)) / (2 * a);
                    result[0] = x1;
                    result[1] = x2;
                    return result;
                }
            }

            /// <summary>
            /// vygeneruje náhodné číslo v rozsahu
            /// </summary>
            /// <param name="min"></param>
            /// <param name="max"></param>
            /// <returns>double</returns>
            public static double Rng(int min, int max)
            {
                Random rand = new Random();
                return rand.Next(min, max + 1);
            }
        }


        public class MathConvertor
        {
            /// <summary>
            /// konvertor desítková -> dvojková 
            /// </summary>
            /// <param name="x"></param>
            /// <returns>string</returns>
            public static string DecToBin(double x)
            {
                string s = "";
                for (int i = 0; x > 0; i++)
                {
                    s += x % 2;
                    x = x / 2;
                }
                return s;
            }

            /// <summary>
            /// konvertor dvojková -> desítková 
            /// </summary>
            /// <param name="x"></param>
            /// <returns>double</returns>
            public static double BinToDec(string x)
            {
                return Convert.ToInt32(x, 2);
            }

            /// <summary>
            /// konvertor desítková -> římská 
            /// </summary>
            /// <param name="cislo"></param>
            /// <returns>string</returns>
            public static string DecToRoman(double cislo)
            {
                if ((cislo < 0) || (cislo > 3999)) throw new ArgumentOutOfRangeException("cislo musi byt mez 1 a 3999");
                if (cislo < 1) return string.Empty;
                if (cislo >= 1000) return "M" + DecToRoman(cislo - 1000);
                if (cislo >= 900) return "CM" + DecToRoman(cislo - 900);
                if (cislo >= 500) return "D" + DecToRoman(cislo - 500);
                if (cislo >= 400) return "CD" + DecToRoman(cislo - 400);
                if (cislo >= 100) return "C" + DecToRoman(cislo - 100);
                if (cislo >= 90) return "XC" + DecToRoman(cislo - 90);
                if (cislo >= 50) return "L" + DecToRoman(cislo - 50);
                if (cislo >= 40) return "XL" + DecToRoman(cislo - 40);
                if (cislo >= 10) return "X" + DecToRoman(cislo - 10);
                if (cislo >= 9) return "IX" + DecToRoman(cislo - 9);
                if (cislo >= 5) return "V" + DecToRoman(cislo - 5);
                if (cislo >= 4) return "IV" + DecToRoman(cislo - 4);
                if (cislo >= 1) return "I" + DecToRoman(cislo - 1);
                throw new ArgumentOutOfRangeException("chyba");
            }

            /// <summary>
            /// konvertor římská -> desítková
            /// </summary>
            /// <param name="cislo"></param>
            /// <returns>double</returns>
            public static double RomanToDec(string cislo)
            {
                cislo = cislo.ToUpper();
                var result = 0;

                foreach (var letter in cislo)
                {
                    result += ConvertLetterToNumber(letter);
                }

                if (cislo.Contains("IV") || cislo.Contains("IX"))
                    result -= 2;

                if (cislo.Contains("XL") || cislo.Contains("XC"))
                    result -= 20;

                if (cislo.Contains("CD") || cislo.Contains("CM"))
                    result -= 200;


                return result;
            }

            /// <summary>
            /// pomocná třída, pímeno na číslo
            /// </summary>
            /// <param name="letter"></param>
            /// <returns>int</returns>
            private static int ConvertLetterToNumber(char letter)
            {
                switch (letter)
                {
                    case 'M':
                        {
                            return 1000;
                        }

                    case 'D':
                        {
                            return 500;
                        }

                    case 'C':
                        {
                            return 100;
                        }

                    case 'L':
                        {
                            return 50;
                        }

                    case 'X':
                        {
                            return 10;
                        }

                    case 'V':
                        {
                            return 5;
                        }

                    case 'I':
                        {
                            return 1;
                        }

                    default:
                        {
                            throw new ArgumentException("neznámé čislo");
                        }
                }
            }
        }
    }
}