﻿using System;

namespace Cvic1.ukol2
{
    class Program
    {
        
        static void Main(string[] args)
        {
            for (int i = 'a'; i <= 'z'; i++)
            {
                Console.Write($"{Convert.ToChar(i)} ");
            }
            Console.Write("\n");
            int w = 'a';
            while (w <= 'z')
            {
                Console.Write($"{Convert.ToChar(w)} ");
                w++;
            }

            Console.Write("\n");
            int d = 'a';
            do
            {
                Console.Write($"{Convert.ToChar(d)} ");
                d++;
            } while (d <= 'z');
        }
    }
}
