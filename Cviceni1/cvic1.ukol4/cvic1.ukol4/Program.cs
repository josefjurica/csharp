﻿using System;

namespace cvic1.ukol4
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Uhádni na 10 pokusů číslo 0-100");          
            Random rnd = new Random();
            int a = rnd.Next(0, 101);
            Console.WriteLine(a);

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Zadej svůj typ pokus: {i + 1}");
                try
                {
                    string b = Console.ReadLine();
                    int c = Convert.ToInt32(b);
                    if (c < a)
                    {
                        Console.WriteLine($"Hledané číslo je větší.");
                    }

                    if (c > a)
                    {
                        Console.WriteLine($"Hledané číslo je menší.");
                    }
                    if (c == a)
                    {
                        Console.WriteLine($"Hledané číslo je nalezeno. Je to {b}.");
                        Console.WriteLine($"Chcete pokračovat? y/n");
                        string d = Console.ReadLine();
                        if (d == "y")
                        {
                            i = -1;
                        }
                        else
                        {
                            Console.WriteLine("Konec");
                            i = 10;
                        }

                    }
                    if (i == 9)
                    {
                        Console.WriteLine($"Hledané číslo je nenalezeno. Bylo to {a}.");
                        Console.WriteLine($"Chcete pokračovat? y/n");
                        string d = Console.ReadLine();
                        if (d == "y")
                        {
                            i = -1;
                        }
                        else
                        {
                            Console.WriteLine("Konec");
                        }
                    }
                }
                catch (Exception)
                {

                    Console.WriteLine("Chyba zadej číslo.");
                }
                
            }

        }
    }
}
